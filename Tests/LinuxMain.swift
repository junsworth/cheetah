import XCTest

import CheetahTests

var tests = [XCTestCaseEntry]()
tests += CheetahTests.allTests()
XCTMain(tests)
