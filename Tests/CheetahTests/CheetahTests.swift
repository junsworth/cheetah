import XCTest
@testable import Cheetah

final class CheetahTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(Cheetah().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
