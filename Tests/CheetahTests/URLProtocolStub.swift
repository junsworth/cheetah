//
//  File.swift
//  
//
//  Created by Jonathan Unsworth on 26/02/2021.
//

import Foundation

class URLProtocolStub: URLProtocol {

    /// The key-pairs of URLs this URLProtocol intercepts with their simulated response
    static var responseStubs: [URL: Result<Data, Error>] = [:]

    override class func canInit(with request: URLRequest) -> Bool {
        guard let url = request.url else {
            preconditionFailure("Unregistered URL: \(request)")
        }
        return responseStubs.keys.contains(url)
    }
    
    override func startLoading() {
        guard let url = request.url, let response = URLProtocolStub.responseStubs[url] else {
            preconditionFailure("Unregistered URL: \(request)")
        }

        handle(response: response)
        
    }

    override func stopLoading() {

    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }

    func handle (response: Result<Data, Error>) {
        switch response {
        case let .success(data):
            /// Simulate received data
            client?.urlProtocol(self, didLoad: data)
            /// Finish loading
            client?.urlProtocolDidFinishLoading(self)
        case let .failure(error):
            /// Simulate error
            client?.urlProtocol(self, didFailWithError: error)
        }
    }

}
