//
//  File.swift
//  
//
//  Created by Jonathan Unsworth on 26/02/2021.
//

import XCTest
@testable import Cheetah

final class URLSessionProviderTests: XCTestCase {

    var provider: URLSessionProvider!
    let baseUrl = URL(string: "http://example.com/networkhosttests/")

    override func setUpWithError() throws {
        try super.setUpWithError()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolStub.self]

        provider = URLSessionProvider(config: config, logger: ConsoleLogger(), decoder: DataDecoderImp())
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        provider = nil
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(Cheetah().text, "Hello, World!")
    }

    func testRequestSuccess() {
        guard let url = baseUrl else {
            return
        }

        URLProtocolStub.responseStubs[url] = .success(Data(repeating: 0, count: 2000))

        let request = URLRequest(url: url)

         _ = provider.load(request, requestType: RequestType.load, type: Data.self) { result in
            if case .success(let data) = result {
                XCTAssert(!data.isEmpty)
            } else {
                XCTFail("Network call failed, got \(result)")
            }
        }

    }

    static var allTests = [
        ("testExample", testExample),
        ("testRequestSuccess", testRequestSuccess),
    ]
}
