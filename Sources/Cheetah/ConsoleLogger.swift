//
//  File.swift
//  
//
//  Created by Jonathan Unsworth on 03/02/2021.
//

import Foundation

public final class ConsoleLogger: NetworkLogger {

    public init() {}
    
    public func willSendRequest(request: URLRequest) {
        print("Sending request:", request.httpMethod ?? "NIL METHOD", request.url ?? "NIL URL")
    }

    public func didReceiveResponse(request: URLRequest, response: HTTPURLResponse, data: Data?) {
        print("Receiving Response:", request.url?.path ?? "NIL PATH", response.statusCode, HTTPURLResponse.localizedString(forStatusCode: response.statusCode).uppercased(), "\(data?.count ?? 0) bytes")
    }

    public func errorOccured(request: URLRequest, response: URLResponse?, error: Error) {
        print("Network Error:", request.url?.path ?? "NIL PATH", error)
    }
}
