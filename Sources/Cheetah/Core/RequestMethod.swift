//
//  File.swift
//  
//
//  Created by Jonathan Unsworth on 06/01/2021.
//

import Foundation

/// Request method types for an HTTP network request

public enum RequestMethod: String {
    case get = "GET"
    case post = "POST"
}
