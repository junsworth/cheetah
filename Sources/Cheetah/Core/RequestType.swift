//
//  File.swift
//  
//
//  Created by Jonathan Unsworth on 09/01/2021.
//

public enum RequestType {
    /// A request that is loaded by a user event and they are actively waiting for a response
    case load
    
    /// A request that is pre-loading data in the background that a user wouldn't notice
    case preload
}
