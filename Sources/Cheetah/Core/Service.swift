//
//  File.swift
//  
//
//  Created by Jonathan Unsworth on 06/01/2021.
//

import Foundation

/// A concrete type that wraps a `URL` & `NetworkProvider` together, which creates a `Service` object allowing you to make network request to a specific domain represented as a `ULR`
open class Service {

    /// The root `domain` url for a given `Service` object, and is configured only once
    let domain: URL
    /// A `NetworkProvider` which is overridden using a concrete implementation
    let networkProvider: NetworkProvider

    private var resourceCache = Dictionary<String, Resource>()
    
    public init(domain: URL, networkProvider: NetworkProvider) {
        self.domain = domain
        self.networkProvider = networkProvider.cheetahNetworkProvider
    }

    // MARK: Service Resource

    public func resource(_ path: String) -> Resource {
        return resource(path: path)
    }

    /// Creates a `Resource` using the `domain` or `base` `URL` for the given `path`
    private final func resource(path: String) -> Resource {
        if var components = URLComponents(url: domain, resolvingAgainstBaseURL: true) {
            components.path = path
            if let url = components.url {
                return Resource(url: url, service: self, encoder: DataEncoderImp())
            }
        }
        return Resource(url: domain, service: self)
    }
}
