//
//  NetworkResponse.swift
//  Cheetah
//
//  Created by Jonathan Unsworth on 07/02/2021.
//

import Foundation

/**
 `NetworkResponse` conforms to `Decodable`, which allows for the easy decoding of data returned inside the body of a network response
 */

/// `NetworkResponse` conforming to `Decodable`, which allows for the easy decoding of data returned inside the body of a network response
public protocol NewtworkResponse: Decodable {}
