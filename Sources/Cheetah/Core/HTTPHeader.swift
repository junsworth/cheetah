//
//  File.swift
//  
//
//  Created by Jonathan Unsworth on 06/01/2021.
//

import Foundation

/// `HTTPHeader` is a centralised struct for http header field types

struct HTTPHeader: RawRepresentable, Equatable, Hashable {
    var rawValue: String
    
    init(rawValue: String) {
        self.rawValue = rawValue
    }
    
    public static let userAgent = HTTPHeader(rawValue: "User-Agent")
    public static let contentType = HTTPHeader(rawValue: "Content-Type")
}
