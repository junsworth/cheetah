//
//  File.swift
//  
//
//  Created by Jonathan Unsworth on 15/01/2021.
//

import Foundation

public struct URLSessionProvider: NetworkProvider {

    let session: URLSession
    let logger: NetworkLogger
    let decoder: DataDecoder
    let acceptableStatusCodes = (200..<300)

    // MARK: ULRSessionProvider Init

    public init(session: URLSession, logger: NetworkLogger, decoder: DataDecoder = JSONDecoder()) {
        self.session = session
        self.logger = logger
        self.decoder = decoder
    }

    public init(config: URLSessionConfiguration, logger: NetworkLogger, decoder: DataDecoder = JSONDecoder()) {
        if #available(iOS 11.0, *) {
            config.waitsForConnectivity = true
        }
        config.timeoutIntervalForResource = 30

        self.session = URLSession(configuration: config)
        self.logger = logger
        self.decoder = decoder
    }

    // MARK: ULRSessionProvider Load

    /**
     Function initiates and returns a `URLSessionDataTask` using the local `URLSession`.
     - Parameter request: The URLRequest for this network request
     - Parameter requestType: The type of network request i.e. `preload` or `load`

     */
    /// Function initiates and returns a `URLSessionDataTask` using the local `URLSession`
    ///
    public func load<T: Decodable>(_ request: URLRequest, requestType: RequestType, type: T.Type, completion: @escaping (Result<T, NetworkError>) -> Void) -> URLSessionDataTask {
        /// Loads and returns a `URLSessionDataTask` using the local `URLSession` object, the data task is responsible
        /// returning the payload of type `T` from the network or a network error `NetworkError`

        var request = request

        if requestType == .preload {
            request.networkServiceType = .background
            if #available(iOS 13.0, *) {
                request.allowsConstrainedNetworkAccess = false
            }
        }

        logger.willSendRequest(request: request)

        let task = session.dataTask(with: request) { (data, response, error) in
            let result: Result<T, NetworkError> = {
                if let error = error {
                    if error.isRequestCancelled {
                        return .failure(.requestCancelled)
                    }
                    self.logger.errorOccured(request: request, response: response, error: error)

                    return .failure(.urlError(error))
                }

                guard let httpURLResponse = response as? HTTPURLResponse else {
                    return .failure(.nonHTTPResponse(response))
                }

                self.logger.didReceiveResponse(request: request, response: httpURLResponse, data: data)

                guard self.acceptableStatusCodes.contains(httpURLResponse.statusCode) else {
                    return .failure(.statusCode(httpURLResponse.statusCode))
                }

                guard let data = data, !data.isEmpty else {
                    return .failure(.emptyResponse)
                }

                do {
                    let decoded = try self.decoder.decode(T.self, from: data)
                    return .success(decoded)
                } catch {
                    return .failure(.decodeError(data, error))
                }

            }()

            completion(result)
        }

        if requestType == .preload {
            task.priority = URLSessionDataTask.lowPriority
        }

        task.resume()

        return task

    }
}
