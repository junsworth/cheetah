//
//  NetworkLogger.swift
//  
//
//  Created by Jonathan Unsworth on 06/01/2021.
//

import Foundation

/// A simple logging interface for capturing network output

public protocol NetworkLogger {
    /// Called before a request is sent to URLSession
    func willSendRequest(request: URLRequest)
    
    /// Called when any data response is received
    func didReceiveResponse(request: URLRequest, response: HTTPURLResponse, data: Data?)
    
    /// Called when URLSession returns an error
    func errorOccured(request: URLRequest, response: URLResponse?, error: Error)
}
