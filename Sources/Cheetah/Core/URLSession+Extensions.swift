//
//  URLSession+Extensions.swift
//  
//
//  Created by Jonathan Unsworth on 05/02/2021.
//

import Foundation

extension URLSession: NetworkProviderContext {
    /// Convenience for creating a `NetworkProvider` using the default implementation of `URLSessionProvider` using the current `URLSession` instance
    public var cheetahNetworkProvider: NetworkProvider {
        return URLSessionProvider(session: self, logger: ConsoleLogger())
    }
}

extension URLSessionConfiguration: NetworkProviderContext {
    /// Convenience for creating a `NetworkProvider` using the default implementation of `URLSessionProvider` using the current `URLSessionConfiguration` instance
    public var cheetahNetworkProvider: NetworkProvider {
        return URLSessionProvider(config: self, logger: ConsoleLogger()).cheetahNetworkProvider
    }
}
