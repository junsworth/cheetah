//
//  File.swift
//  
//
//  Created by Jonathan Unsworth on 06/01/2021.
//

import Foundation

/**
 The `NetworkError` enum object, detailing the acceptable network error states
 */

/// `NetworkError` In case of network error, `NetwrokError` is used

public enum NetworkError: Error {
    /// An error was returned by URLSession
    case urlError(Error)
    
    /// The request was cancelled
    case requestCancelled
    
    /// The response was not an http response
    case nonHTTPResponse(URLResponse?)
    
    /// The request responded with http status code
    case statusCode(Int)
    
    /// The request response data is empty
    case emptyResponse
    
    /// An error was returned by the decoder
    case decodeError(Data, Error)
    
    /// An error was returned by the encoder
    case encodeError(Error)
}

extension Error {
    /// Boolean flag representing the cancelled state of the request
    var isRequestCancelled: Bool {
        let error = self as NSError
        return error.domain == NSURLErrorDomain && error.code == NSURLErrorCancelled
    }
}
