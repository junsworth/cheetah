//
//  File.swift
//  
//
//  Created by Jonathan Unsworth on 06/01/2021.
//

import Foundation

public protocol DataDecoder {
    func decode<T: Decodable>(_ type: T.Type, from data: Data) throws -> T
}

extension JSONDecoder: DataDecoder {}

public struct DataDecoderImp: DataDecoder {
    public func decode<T: Decodable>(_ type: T.Type, from data: Data) throws -> T {
        guard let decoded = data as? T else {
            throw DecodingError.dataCorrupted(DecodingError.Context(codingPath: [], debugDescription: "\(self) cannot decode data as \(T.self)"))
        }
        return decoded
    }
}
