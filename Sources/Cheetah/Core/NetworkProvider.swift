//
//  File.swift
//  
//
//  Created by Jonathan Unsworth on 15/01/2021.
//

import Foundation

/**
 `NetworkProvider` is an abstract interface that allows different network libraries to add their concrete implementation for when lading data from a network source
 */

public protocol NetworkProvider: NetworkProviderContext {
    /// Function is responsible for loading data from from a given network resource, using the given/supplied concrete implementation
    func load<T: Decodable>(_ request: URLRequest, requestType: RequestType, type: T.Type, completion: @escaping (Result<T, NetworkError>) -> Void) -> URLSessionDataTask
}

extension NetworkProvider {
    /// Pass a concrete implementation of the `NetworkProvider` when creating a `Service` to override default behaviour.
    public var cheetahNetworkProvider: NetworkProvider {
        return self
    }
}

/// Used by a `NetworkingProvider` implementation to pass the result of a network request back to Siesta.
public typealias RequestNetworkingCompletionCallback = (HTTPURLResponse?, Data?, Error?) -> Void

public typealias NetworkRequestCompletionHandler<T> = (Result<T, NetworkError>) -> Void

/**
 A convenience to create the appropiate `NetworkProvider` for a variety of networking configuaration objects.
 */
public protocol NetworkProviderContext {
    /// Returns the appropiate `NetworkProvider`
    var cheetahNetworkProvider: NetworkProvider { get }
}
