//
//  NetworkRequest.swift
//  Cheetah
//
//  Created by Jonathan Unsworth on 06/02/2021.
//

import Foundation

/// `NetworkRequest` conforming to `Encodable`, which allows for the easy encoding of data and assignment to the `httpBody` field of a `URLRequest` object
/**

 `NetworkRequest` conforming to `Encodable`, which allows for the easy encoding of data and assignment to the `httpBody` field of a `URLRequest` object

 */

public protocol NetworkRequest: Encodable {}
