//
//  DataEncoder.swift
//  Cheetah
//
//  Created by Jonathan Unsworth on 06/02/2021.
//

import Foundation

public protocol DataEncoder {
    func encode<T: Encodable>(_ value: T) throws -> Data
}

extension JSONEncoder: DataEncoder {}

public struct DataEncoderImp: DataEncoder {
    public func encode<T: Encodable>(_ value: T) throws -> Data {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted

        guard let data = try? encoder.encode(value) else {
            throw EncodingError.invalidValue(value, EncodingError.Context(codingPath: [], debugDescription: "\(self) cannot encode \(T.self) as data"))
        }
        print(String(data: data, encoding: .utf8)!)
        return data
    }
}
